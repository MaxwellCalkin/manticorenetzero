const express = require('express')
const app = express()
const cors = require('cors')
const MongoClient = require('mongodb').MongoClient
require('dotenv').config()
const PORT = 8000

let db,
    dbConnectionString = process.env.DB_STRING,
    dbName = 'manticore',
    collection

MongoClient.connect(dbConnectionString)
    .then(client => {
        console.log('Connected to Database')
        db = client.db(dbName)
        collection = db.collection('netZero')
    })

app.set('view_engine', 'ejs')
app.use(express.static('public'))
app.use(express.urlencoded({extended:true}))
app.use(express.json())
app.use(cors())


app.get('/', async (req,res) => {
    try{
        res.render('index.ejs')
    } catch {
        res.status(500).send({message: error.message})
    }
})
app.get('/results', async (req,res) => {
    try{
        res.render('results.ejs')
    } catch {
        res.status(500).send({message: error.message})
    }
})

app.post('/results', (req,res) => {
    console.log('post heard', req.body)
    db.collection('netZero').insertOne(
        req.body
    )
    .then(result => {
        console.log(result)
        res.redirect('/results')
    })
    .catch(error => console.error(error))
})


app.listen(process.env.PORT || PORT, () => {
    console.log(`Server is running on port ${process.env.PORT}`)
})